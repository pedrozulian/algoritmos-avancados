import pandas as pd
import numpy as np


def criar_lista():
    lista = []
    n = int(input("Quantidade de vértices: "))
    for i in range(n):
        lista.append([i])
    return lista

def imprime_lista(lista):
    for v in lista:
        print(v)

def insere_aresta(v1, v2, lista):
    lista[v1].append(v2)
    lista[v2].append(v1)
    imprime_lista(lista)


def vertice_adjascente(v, lista):
    adj = []
    for i in range(len(lista)):
        if (lista[i][0] == v):
            adj = lista[i][1:]
    return adj

def grau_vertice(v, lista):
    grau = len(vertice_adjascente(v, lista))
    for vizinho in vertice_adjascente(v, lista):
        if (vizinho == v):
            grau += 1
    return grau

def existe_aresta(v1, v2, lista):
    v1 = int(input("Vetor1: "))
    v2 = int(input("Vetor2: "))
    if(v1 in lista[v2][1:]) or (v2 in lista[v1][1:]):
        return True
    else:
        return False

print('''
1. Criar lista
2. Visualizar grafo (todas arestas)
3. Inserir aresta
4. Visualizar vértice adjascente
5. Verifica aresta entre vetores
6. Cacular grau do vértice''')

grafo = []

while True:
    opcao = int(input("\nDigite a opção: \n"))

    if opcao == 1:
        grafo = criar_lista()
    elif opcao == 2:
        imprime_lista(grafo)
    elif opcao == 3:
        v1 = int(input("\nDigite o vetor1: "))
        v2 = int(input("Digite o vetor2: "))
        insere_aresta(v1, v2, grafo)