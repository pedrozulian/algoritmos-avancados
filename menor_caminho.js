grafo = [
    { 0: [ {1: 0}, {3: 0} ] },
    { 1: [ {3: null}, {4: null} ] },
    { 2: [ {0: null}, {5: null} ] },
    { 3: [ {2: null}, {4: null}, {5: null}, {6: null} ] },
    { 4: [ {6: null} ] },
    { 5: [ ] },
    { 6: [ {5: null} ] }
]

const verticeAdj = (vertice=null) => {
    return grafo[vertice]
}

const verticeAdjUnidade = (vertice=null) => {
    let adj = [];
    adj = verticeAdj(vertice)
    console.log(adj)
    for (vizinho in adj) {
        console.log(vizinho)
    }
}

console.log(verticeAdjUnidade(0))


function buscaProfundidade(verticeIncial=null) {
    let x = verticeIncial;
    const fila = [x];
    const visitados = [x];
    while (fila.length) {
        n = fila.pop(0)
        const m = verticeAdjUnidade(n)
        console.log(m)
    }
}

// console.log(buscaProfundidade(0))