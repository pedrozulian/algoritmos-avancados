# grafo a

# lista_vertices = [
#     [0, 1, 2],
#     [1, 0, 2],
#     [2, 0, 1],
#     [3, 4],
#     [4, 3],
#     [5, 6, 8],
#     [6, 5, 7, 8],
#     [7, 6, 8],
#     [8, 5, 6, 7]
# ]

# grafo b

lista_vertices = [
    [0, 1],
    [1, 0],
    [2],
    [3],
    [4, 5],
    [5, 4],
    [6],
]

def criar_grafo():
    n = int(input("********************\nQtde. de vértices: "))
    cont = 0
    while cont < n:
        novo_vertice = []
        novo_vertice.append(cont)
        lista_vertices.append(novo_vertice)
        cont += 1
    return lista_vertices


def imprime_lista(lista_vertices):
    print("********************\nGrafo: ")
    for v in lista_vertices:
        print(v)


def insere_aresta(v1, v2, lista_vertices):
    lista_vertices[v1].append(v2)
    lista_vertices[v2].append(v1)
    imprime_lista(lista_vertices)


def verticesAdj(lista_vertices, vertice=None):
    if vertice == None:
        x = int(input("********************\nInforme o Vertice: "))
    else:
        x = vertice
    adj = []
    for i in range(1, len(lista_vertices[int(x)])):
        adj.append(lista_vertices[int(x)][i])
    return adj



def encontra_caminho(vertice, caminhos=[], visitados=[]):
    proximo_caminho = ()
    visitados.append(vertice)
    for vizinho in verticesAdj(lista_vertices, vertice):
        if (vizinho not in visitados):
            proximo_caminho = (vertice, vizinho)
    if (not proximo_caminho): return caminhos, visitados
    caminhos.append(proximo_caminho)
    return encontra_caminho(proximo_caminho[1], caminhos, visitados)


def encontra_componente(vertices=[]):
    caminhos = []
    componentes = 0
    index_vertices = [x for x in range(len(vertices))]
    while len(index_vertices) > 0:
        caminho, visitados = encontra_caminho(index_vertices[0], [], [])
        componentes += 1
        # print(f'Começando do vetor: {index_vertices[0]} = {caminho}')
        nao_visitados = [x for x in index_vertices if x not in visitados]
        index_vertices = nao_visitados
    print("Quantidade de componentes: ", componentes)



print('''
---------------------
1. Criar grafo
2. Inserir aresta
3. Visualizar grafo
4. Encontrar componentes
---------------------
''')

while True:
    opcao = int(input("\n*************************\nDigite a opção: \n"))

    if opcao == 1:
        grafo = criar_grafo()
    elif opcao == 2:
        v1 = int(input("\nDigite o vetor 1: "))
        v2 = int(input("Digite o vetor 2: "))
        insere_aresta(v1, v2, grafo)
    elif opcao == 3:
        imprime_lista(grafo)
    elif opcao == 4:
        encontra_componente(lista_vertices)