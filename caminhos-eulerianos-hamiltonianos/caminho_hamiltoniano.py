##### grafo exemplo

grafo = [
    [0, 1, 3, 4],
    [1, 0, 4, 2],
    [2, 1, 4, 3],
    [3, 0, 4, 2],
    [4, 0, 1, 2, 3]
]

# grafo = [
#     [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
#     [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
#     [2, 3, 4, 5, 6, 7, 8, 9, 0, 1],
#     [3, 4, 5, 6, 7, 8, 9, 0, 1, 2],
#     [4, 5, 6, 7, 8, 9, 0, 1, 2, 3],
#     [5, 6, 7, 8, 9, 0, 1, 2, 3, 4],
#     [6, 7, 8, 9, 0, 1, 2, 3, 4, 5],
#     [7, 8, 9, 0, 1, 2, 3, 4, 5, 6],
#     [8, 9, 0, 1, 2, 3, 4, 5 ,6, 7],
#     [9, 0, 1, 2, 3, 4, 5, 6, 7, 8]
# ]



def verticesAdj(grafo, vertice=None):
    if vertice == None:
        x = int(input("Informe o Vertice: "))
    else:
        x = vertice
    adj = []
    for i in range(1, len(grafo[int(x)])):
        adj.append(grafo[int(x)][i])
    return adj
            

def encontra_caminho(vertice, caminhos=[], visitados=[]):
    proximo_caminho = ()
    visitados.append(vertice)
    for vizinho in verticesAdj(grafo, vertice):
        if (vizinho not in visitados):
            proximo_caminho = (vertice, vizinho)
    if (not proximo_caminho): return caminhos
    caminhos.append(proximo_caminho)
    return encontra_caminho(proximo_caminho[1], caminhos, visitados)

print(encontra_caminho(0))