# grafo de exemplo

grafo = [
   [0, 1, 4, 3],
   [1, 0, 2, 3, 4],
   [2, 1, 3],
   [3, 2, 4, 0 , 1],
   [4, 3, 0, 1]
]   

# grafo de konigsberg

# grafo = [
#     [0, 2, 1, 3, 1, 3],
#     [1, 0, 0, 1],
#     [2, 1, 0, 3],
#     [3, 0, 0, 2]
# ]

pares = []
impares = []

def valida_caminho_euleriano(grafo):
    for i in grafo:
        if len(i[1:]) % 2 == 0:
            pares.append(i)
        else:
            impares.append(i)
    if len(impares) == 2:
        print('Existe um caminho Euleriano.')
    else:
        print('Não existe um caminho Euleriano.')

    
def verticesAdj(grafo, vertice=None):
    if vertice == None:
        x = int(input("Informe o Vertice: "))
    else:
        x = vertice
    adj = []
    for i in range(1, len(grafo[int(x)])):
        adj.append(grafo[int(x)][i])
    return adj


def encontrar_caminho(vertice, caminhos=[]):
    proximo_caminho = ()
    for vizinho in verticesAdj(grafo, vertice):
        possivel_caminho = (vertice, vizinho)
        caminho_inverso = (vizinho, vertice)
        if (possivel_caminho in caminhos) or (caminho_inverso in caminhos):
            pass
        else:
            proximo_caminho = possivel_caminho
            break
    if (not proximo_caminho): return caminhos
    caminhos.append(proximo_caminho)
    return encontrar_caminho(proximo_caminho[1], caminhos)


print(encontrar_caminho(0))
print(valida_caminho_euleriano(grafo))
