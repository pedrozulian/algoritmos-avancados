# Função recursiva para calcular o valor de x multiplicado por y.


def multiply(x, y):
    if y == 1:
        return x
    else:
        return x + multiply(x, y - 1)

print(multiply(3, 2))


# Função recursiva para calcular o valor de x elevado a y

def exponential(x, y):
    if y == 1:
        return x
    else:
        return x * exponential(x, y - 1)

print(exponential(5, 3))
