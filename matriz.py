import pandas as pd
import numpy as np



def criarMatriz():
    qtdVertices = int(input("Digite a quantidade de vértices: "))
    matriz = pd.DataFrame(np.zeros((qtdVertices,qtdVertices)), dtype=int)
    print(matriz)
    return matriz

#####

def visualizarMatriz(matriz):
    print(matriz)

# Inserir uma aresta entre dois vertices

def insereAresta(matriz):
    v1 = int(input('Vertice origem: '))
    v2 = int(input('Vertice destino: '))
    matriz.loc[v1, v2] = 1
    matriz.loc[v2, v1] = 1

# Consulta se existe uma aresta entre os 2 vertices

def consultaArestas(consultv1, consultv2):
    consult = grafo._get_value(consultv1, consultv2)
    return consult

# # Retorna os vertices adjascentes de determinado vetor

def verticesAdjascentes(v, grafo):
    adj = []
    for i in range(len(grafo)):
        if(grafo[v][i] == 1):
            adj.append(i)
    return adj

# Visualiza o grau do Vértice

def grauVertices(v, matriz):
    if(matriz[v][v] == 1):
        return len(verticesAdjascentes(v, matriz)) + 1
    else:
        return len(verticesAdjascentes(v, matriz))


print('''**************************************************
[ 1 ] Criar Matriz
[ 2 ] Inserir Arestas
[ 3 ] Visualizar Matriz
[ 4 ] Vertices Adjascentes (vizinhos)
[ 5 ] Verificar existencia de aresta
[ 6 ] Grau do vértice
**************************************************
''')

while True:
    opcao = int(input('''Digite a opção: '''))
    if opcao == 1:
        grafo = criarMatriz()
    elif opcao == 2:
        insereAresta(grafo)
    elif opcao == 3:
        visualizarMatriz(grafo)
    elif opcao == 4:
        v = int(input("Informe o vértice: "))
        adj = verticesAdjascentes(v, grafo)
        print("Vertices adjascentes de", v," são => ", adj)
    elif opcao == 5:
        print("Existe aresta entre: \n")
        consultv1 = int(input('Vertice 1: '))
        consultv2 = int(input("Vertice 2: "))
        res = consultaArestas(consultv1, consultv2)
        print(res)
    elif opcao == 6:
        v = int(input("Informe o vértice: "))
        grau = grauVertices(v, grafo)
        print("O grau do vértice ", v, " é ", grau)