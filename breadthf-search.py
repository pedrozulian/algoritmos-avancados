# Breadth-First Search - busca em largura

def verticesAdj(grafo, vertice=None):
    if vertice == None:
        x = int(input("Informe o Vertice: "))
    else:
        x = vertice
    adj = []
    for i in range(1, len(grafo[int(x)])):
        adj.append(grafo[int(x)][i])
    return adj

def buscaLargura(grafo, verticeInicial=None):
    if verticeInicial == None:
        x = int(input("Informe o vertice: "))
    else:
        x = verticeInicial
    fila = [x]
    visitados = [x]
    while len(fila):
        n = fila.pop(0)
        for m in verticesAdj(grafo, n):
            if m not in visitados:
                visitados.append(m)
                fila.append(m)
    return visitados

grafo = [
    [0, 1, 2],
    [1, 0, 3, 4],
    [2, 0, 5, 6],
    [3, 1],
    [4, 1],
    [5, 2],
    [6, 2]
]

print(buscaLargura(grafo, 0))