# 1) Função recursiva que retorna o primeiro algarismo (o mais significativo) na representação decimal de N.

def get_first(n):
    if (n < 10):
        return n
    else:
        return get_first(n // 10)

print(get_first(532))

# 2) Função recursiva que recebe como parâmetro uma lista de números inteiros L e um número inteiro N e retorna a lista 
# que resulta de apagar de L todas as ocorrências de N.

def remove_n(lista, n):
    if n not in lista:
        return lista
    else: 
        lista.remove(n)
        return remove_n(lista, n)

print(remove_n([1, 2, 3], 2))



# 3) Função que ao passar o parâmetro, retorna se esse número é um número perfeito ou não.

def perfect_n(n, cont = 1, soma = 0):
    if (cont == n):
        return (soma == n)
    if (n % cont == 0):
        soma += cont
    return perfect_n(n, cont +1, soma)

print("Número perfeito: ", perfect_n(6))