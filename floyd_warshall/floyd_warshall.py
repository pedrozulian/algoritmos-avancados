import pandas as pd
import numpy as np

grafo = [
    [[1, 0], [3, -2]],
     [[2, 0], [1, 4], [3, 3]],
     [[3, 0],[4, 2]], 
     [[4, 0], [2, -1]]
]

# grafo_exemplo = [
#     [[1, 0], [2, 2], [3, 10], [4, 12]],
#     [[2, 0], [1, 2], [4, 2], [6, 17], [5, 25]],
#     [[3, 0], [1, 10], [4, 4], [5, 17]],
#     [[4, 0], [1, 12], [2, 2], [3, 4], [5, 19]],
#     [[5, 0], [2, 25], [3, 17], [4, 19]],
#     [[6, 0], [2, 17]]
# ]

def get_vertex(grafo):
    list = []
    for v in grafo:
        list.append(v[0][0])
    return list

def get_distance(matriz, i, j):
    distance = matriz.loc[i, j]
    return distance

def create_matriz(size, grafo):
    matriz = np.matrix(np.ones((size, size)) * np.inf)
    labels = get_vertex(grafo)
    matriz = pd.DataFrame(matriz, index=labels, columns=labels, dtype=float)
    return matriz

def matriz_w_distances(grafo):
    matriz = create_matriz(len(grafo), grafo)
    for vertice in grafo:
        print(matriz)
        i = vertice[0][0]
        for neighbor in vertice:
            j = neighbor[0]
            d = neighbor[1]
            matriz[j][i] = d
    return matriz


def get_distance(matriz, i, j):
    distance = matriz.loc[i, j]
    return distance

def insert_distance(matriz, i, j, d):
    matriz.loc[i, j] = d
    return matriz


def warshall(grafo):
    matriz = matriz_w_distances(grafo)
    k = 0
    list_vertex = get_vertex(grafo)
    for k in list_vertex:
        for i in list_vertex:
            i = i
            for j in list_vertex:
                j = j
                dist_ij = get_distance(matriz, i, j)
                dist_ik = get_distance(matriz, i, k)
                dist_kj = get_distance(matriz, k, j)
                if (dist_ij > dist_ik+dist_kj):
                    new_dist = dist_ik+dist_kj
                    matriz = insert_distance(matriz, i, j, new_dist)
        k +=1
    return matriz

print(warshall(grafo))