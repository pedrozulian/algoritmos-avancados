## Depth-First Search - busca em profundidade

grafo = [
    [0, 1, 2],
    [1, 0, 3, 4],
    [2, 0, 5, 6],
    [3, 1],
    [4, 1],
    [5, 2],
    [6, 2]
]

def verticesAdj(grafo, vertice=None):
    if vertice == None:
        x = int(input("Informe o Vertice: "))
    else:
        x = vertice
    adj = []
    for i in range(1, len(grafo[int(x)])):
        adj.append(grafo[int(x)][i])
    return adj


def buscaProfundidade(vertice, visitados=[]):
    if vertice in visitados:                                    # se o vertice estiver em visitados
        return visitados                                        # retorna a lista de visitados
    visitados.append(vertice)                                   # se não, visitados pega o vertice
    for vizinho in verticesAdj(grafo, vertice):                 # para o vizinho do vertice "0", pega o primeiro vizinho
        if vizinho not in visitados:                            # se este vizinho "1" não estiver na lista visitados
            visitados = buscaProfundidade(vizinho, visitados)   # vamos inserir na lista visitados, e buscar o próximo vizinho de "1"
    return visitados                                            # se todos os vertices estiverem em visitados, retorna a lista


print(buscaProfundidade(0))
    

